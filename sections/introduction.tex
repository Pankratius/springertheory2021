\section{Introduction}
\subsection{Recollection}
\begin{numtext}
  We first recall where we stand and set some notation and names:
  Let $\ggroup$ be a fixed algebraic group (in our case, mostly $\slgroup{n}$), and let $\glie$ be its Lie algebra.
  Associated to $\ggroup$, we have the \emph{flag variety} $\bvar$, i.e. the $\ggroup$-homogenous projective varity parametrizing the Borel subgroups of $\ggroup$. After choosing a Borel subgroup $\bogroup\sse \ggroup$, we can identify it with $\ggroup/\bogroup$.
  We denote by $\nvar\sse \glie$ the \emph{nilpotent cone}, which is the subvariety of all nilpotent elements, and also define
  \[
  \ntilde\defined
  \lset (e,\blie)\ssp e\in \nvar,~\blie \in \bvar,~x\in \blie\rset.
  \]
  The projection onto the first factor $\mu \mc \ntilde\to \nvar$ is called the \emph{Springer resolution}.
  In fact, $\ntilde$ is a smooth variety and the map $\mu$ is a resolution of singularities and furthermore \emph{semismall}, which means that for all $d\geq 1$, the following condition on the fibres of $\mu$ holds:
  \begin{equation}\label{setup:semismall}\tag{SeSm}
    \codim_Y \left(\lset y\in Y\ssp \dim \mu^{-1}(y)\geq d\rset\right) \leq 2d
  \end{equation}
  The map $\mu$ gives rise to all other geometric objects of interest to us, first and foremost the fibre product $\zvar\defined \ntilde\times_{\nvar}\ntilde$, which is called the \emph{Steinberg variety}.
  In the last talk, we saw that the highest Borel-Moore homology with coefficients in $\rationals$ of the Steinberg variety can be identified with the group algebra of the Weyl group $\wgroup$ of $\ggroup$, i.e. that there is an algebra isomorphism
  \begin{equation}\label{steinberghomology}
  \hlgy(\zvar)\isomorphism \rationals[\wgroup].
  \end{equation}
  Secondly, associated to a point $e\in \nvar$, we denote by $\bvar_e$ its fibre under $\mu$. It is the closed subvariety of $\ntilde$ consisting of all Borel algebras of $\glie$ that contain $e$.\par
\end{numtext}
In order to make matters more conrecte, we establish the following examples:
\begin{example}
  .
\end{example}
\begin{rem}
  Some authors (e.g. \cite{clausen}) work with a ``group version'' of our setup. That is, instead of working with $\glie$ and $\nvar$, they use the group $\ggroup$ itself and the \emph{unipotent variety} or \emph{unipotent locus} $\uvar\sse \ggroup$, which is defined to be the subgroup of $\ggroup$ consisting of all unipotent elements (at least in characterstic zero, for the general case, see e.g. \cite{milne-algebraic-groups}*{14.b}).
  The group $\ggroup$ acts on $\uvar$ by conjugation.
  There are also analogous constructions to the Springer resolution and the Steinberg varieties.
  In the case of characterstic zero, the exponential map yields a $\ggroup$-equivariant isomorphism of varieties $\nvar\isomorphism \uvar$, and so it allows us to identify these constructions.
  In positive characterstic, the situation is more subtle, see for example \cite{springer1969unipotent} or \cite{sobaje2015springer}.
\end{rem}

\subsection{The component group}
Before we can state the main theorem of the talk, we need to account for certain connectedness properties of centralizer associated to unipotent/nilpotent elements.
\begin{defn}
\leavevmode
\begin{enumerate}
\item
    Let $g\in \ggroup$, then the \emph{centralizer of $g$ in $\ggroup$} is defined as
    \[
    C_{\ggroup}(g) \defined \lset h\in \ggroup \ssp ghg^{-1}= h\rset.
    \]
\item
    For an element $x$ in the Lie algebra $\glie$ of $\ggroup$, we define its \emph{global centralizer in $\ggroup$ as}
    \begin{align*}
    C_{\ggroup}(x) &\defined \lset g\in \ggroup \ssp \left(\adaction g\right)(x) = x\rset
    \\
    \intertext{and its \emph{infinitesimal centralizer in $\glie$} as}
    \clie_{\glie}(x)&\defined \lset y\in \glie \ssp [y,x] = 0 \rset
    \end{align*}
  \end{enumerate}
\end{defn}
In characterstic zero, these notions agree (after taking the Lie algebra or the exponential respecitvley, see \cite{humphreys-conjugacy}*{1.10}).
\begin{construction}
  Let $u\in \ggroup$ be unipotent, and denote by $C_{\ggroup}^\circ(u)\sse C_{\ggroup}(u)$ the connected component of the identity element $1\in \ggroup$. Then $u\in C_{\ggroup}^\circ$:
  Indeed, the quotient $C_{\ggroup}(u)/C_{\ggroup}^\circ$ is finite, and the image of $u$ is again unipotent.
  Since in characterstic zero there are no unipotent elements of finite order, the image of $u$ has to be trivial.\par
  We define the \emph{component group} of $u$ to be this quotient $A_u\defined C_{\ggroup}(u)/C_{\ggroup}(u)^\circ$.
\end{construction}
\begin{rem}
As with the centralizer, the construction of the component group is flexible with the usual operations we would like to perform:
\begin{enumerate}
  \item The component type of $\ggroup$ does not depend on the isogeny type of $\ggroup$ in the following sense:
  If $\pi\mc \ggroup\to \ggroup'$ is a central isogeny, then its kernel consists of semisimple elements, hence it induces an isomorphism of the unipotent varieties $\pi\mc \uvar\isomorphism \uvar'$ and thus $C_{\ggroup'}(\pi(u)) \cong \pi(C_{\ggroup}(u))$.
  This observation allows us to state most examples of component groups of any root type by considering the \emph{group of adjoint type} (meaning the center is trivial).
  \item For a nilpotent element $e\in \glie$, we define the component group of $e$ to be $C_{\ggroup}(\exp(e))/C_{\ggroup}(\exp)^\circ$, which is the same as $A_{\exp(e)}$. So we will also just write $A_{e}$.
   \end{enumerate}
\end{rem}

\begin{example}
  \leavevmode
  \begin{enumerate}
    \item In $\glgroup{n}$, every centralizer corresponds to a principal open subset, and these are always connected. In particular, for every element $u\in \uvar$, the component group is trivial.
    \item Assume that $\ggroup$ is of adjoint type and let $e\in \ggroup$ be a regular nilpotent element.
    Then the centralizer $C_{\ggroup}(e)$ is connected and the component group is again trivial \cite{springer-connected}*{(4.11)}.
    In the case that $e$ is subregular, \cite{slodowysimple} gives a full description of the component groups, broken down by root type:
    \begin{table}[h]
    \label{introduction:component-groups}
    \centering
    \begin{tabular}{c||c|c|c|c|c|c|c}
    $\Delta$
    &
    $A_r$ ($r>1$)
    &
    $B_r$
    &
    $C_r$
    &
    $D_r$
    &
    $E_r$
    &
    $F_4$
    &
    $G_2$
    \\
    \hline
    $A_e$
    &
    $\ggroup_m$
    &
    $\ggroup_m \rtimes \zz/2\zz$
    &
    $\zz/2\zz$
    &
    $\lset 1\rset$
    &
    $\lset 1\rset$
    &
    $\zz/2\zz$
    &
    $\sgroup{3}$
    \end{tabular}
    \end{table}
    \par
    For an explanation of this result for $G_2$, see \cite{humphreys-conjugacy}*{7.18}.
  \end{enumerate}
\end{example}
Our next goal is to see that the component group acts on the homology of the Springer fibre:
